import De from "./de";

export default class Gobelet {
    _des: De[]

    constructor(nbJoueurs: number, type2D: number){
        for( let i = 1; i < nbJoueurs; i++){
            this._des.push( new De(type2D));
        }
    }
    lancer(): number[]{
        let max: number = 0;
        let gagnants: number[] = [];
        this._des.forEach((de, index) => {
            de.lancer();
            if (de.valeur == max){
                gagnants.push(index);
            }else if (de.valeur > max){
                max = de.valeur;
                gagnants = [index];
            }
        });
        this.afficherScore();
        return gagnants;
    }
    afficherScore(){
        let result;
        this._des.forEach( (de, index) => {
            result += `Dé ${index+1} = ${de.valeur}, `;
        });
        console.log(result);
    }
}