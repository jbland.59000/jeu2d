import Gobelet from "./gobelet";

export default class Joueur {
    _nom: string;
    _score: number;

    constructor(nom: string){
        this._nom = nom;
    }
    jouer(gobelet: Gobelet): number[]{
        return gobelet.lancer();
    }

    afficherScore(){
        console.log(`Joueur ${this._nom} , score : ${this._score}`);
    }
    reset(){
        this._score = 0;
    }
    ajouterPoint(){
        this._score += 1;
    }
    get score(){
        return this._score;
    }
    get nom(){
        return this._nom;
    }
}