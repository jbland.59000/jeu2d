import Gobelet from "./gobelet";
import Joueur from "./joueur";

export default class Partie {
    _nbTours: number;
    _joueurs: Joueur[];
    gobelet: Gobelet;
    _ready: boolean = false;

    initialiserPartie(type2d: number = 6){
        if(this._nbTours > 2){
            this._ready = true;
            console.log('Il manque des Joueurs');
        }else{
            this._ready = true;
            this.gobelet = new Gobelet(this._joueurs.length, type2d);
            this._joueurs.forEach( (joueur, index) => {
                joueur.reset();
                console.log( `${joueur.nom} est le joueur ${index}`);
            })
        }
    }
    lancerPartie(){
        if ( !this._ready){
            console.log("La partie n'est pas prete ..");
            return;
        }
        let gagnants: number[] = []
        for(let i = 1; i < this._nbTours; i++){
            this._joueurs.forEach( joueur => {
                gagnants = joueur.jouer(this.gobelet);
                gagnants.forEach(gagnant =>{
                    this._joueurs[gagnant].ajouterPoint();
                })
            })
        }
        this.afficherGagnant();
    }
    ajouterJoueur(nom: string){
        this._ready = false;
        this._joueurs.push(new Joueur(nom));
        this._nbTours = this._joueurs.length +1;
    }
    afficherGagnant(){
        let max: number = 0;
        let gagnants: number[] = [];
        this._joueurs.forEach((joueur, index) => {
            joueur.afficherScore();
            if (joueur.score == max){
                gagnants.push(index);
            }else if (joueur.score > max){
                max = joueur.score;
                gagnants = [index];
            }
        });
        if( max == 0){
            console.log("La partie n'a pas encore été lancée .");
            return;
        }
        let tmp: string = '';
        gagnants.forEach( gagnant => {
            if( tmp == ''){
                tmp += `${this._joueurs[gagnant].nom}`
            }else{
                tmp += `, ${this._joueurs[gagnant].nom}`
            }
        });
        tmp += gagnants.length > 1 ? ' sont ex aequo !' : ' à gagné la partie !';
        console.log(tmp);
        this._ready = false;
    }
}