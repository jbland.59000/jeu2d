 export default class De {
     _valeur: number;
     _valeurMax: number

     constructor(valeurMax: number){
         this._valeurMax = valeurMax;
     };

     lancer(){
         this._valeur = Math.floor(Math.random() * (this._valeurMax - 1) +1);
     }

     get valeur(){
         return this._valeur;
     }
 }